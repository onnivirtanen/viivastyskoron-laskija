import { describe } from 'mocha';
import { expect } from 'chai';
import Koronlaskija from '../src/korko.mjs';

describe('Korko test suite', () => {
    it('Can calculate viivastyskorko', () => {
        const korkopaivat = 25;
        const korko = 8;
        const viivastyskorko = '0.43';
        expect(Koronlaskija.laskeViivastyskorko(25, 8, 78)).to.equal(viivastyskorko);
    });
});
