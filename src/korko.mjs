export default class Koronlaskija {
 
    static laskeViivastyskorko(korkopaivat, korkoprosentti, paaoma) {
        const paivaaVuodessa = 365; // ei huomioida karkausvuosia
        return ((korkopaivat * korkoprosentti * paaoma) / (100 * paivaaVuodessa)).toFixed(2);
    }

}