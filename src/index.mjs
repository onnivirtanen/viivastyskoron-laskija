import Koronlaskija from "./korko.mjs";

const laskeViivastyskorko = () => {
    const paaoma = parseFloat(document.getElementById('paaoma').value);
    const korkopaivat = parseFloat(document.getElementById('korkopaivat').value);
    const korkoprosentti = parseFloat(document.getElementById('korkoprosentti').value);
    const yhteensa = document.getElementById('yhteensa');
    
    yhteensa.value = Koronlaskija.laskeViivastyskorko(korkopaivat, korkoprosentti, paaoma);
}
 
const setup = () => {
    document.getElementById('paaoma').onchange = () => laskeViivastyskorko();
    document.getElementById('korkopaivat').onchange = () => laskeViivastyskorko();
    document.getElementById('korkoprosentti').onchange = () => laskeViivastyskorko();   
}

setup();